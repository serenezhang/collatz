class Cache:
    def __init__(self, n=1000, min_n=0, max_n=1000000, lazy={}):
        self.n = n
        self.min_n= min_n
        self.max_n = max_n
        # Cache format {str:int}
        # {"range:range+1":max_cycle_length} for ranges
        # {"n":v} for indivudal numbers
        self.cache = {}

        if lazy != {}:
            self.cache.update(lazy)
        else:
            self.populateCache()

    def get(self, n):
        # Edge case for 1
        if n == "1":
            return 1
    
        result = str(n) in self.cache
        # Key not present
        if not result:
            return False
        # Key present
        else:
            return self.cache[str(n)]
        
    def set(self, k, v):
        self.cache[k] = v

    def populateCache(self):
        for i in range(self.min_n, self.max_n+1, self.n):
            # Edge case so naming is consistent
            if i == 0:
                i = 1
            v = self.collatz_eval(i, i+self.n)
            self.cache[f"{i}-{i+self.n}"] = v

    def printCache(self):
        counter = 1
        e = " "
        for key, value in self.cache.items():
            if counter % 5 == 0:
                e = "\n"
            print(f"'{key}': {value},", end=e)
            counter += 1
            e = " "
            
    def collatz_eval(self, i, j):
        """
        i the beginning of the range, inclusive
        j the end       of the range, inclusive
        return the max cycle length of the range [i, j]
        """
        maxC = 0

        for n in range(i, j+1):
            v = self.collatz_cycle_length(n)
            if v > maxC:
                maxC = v

        return maxC
    
    def collatz_cycle_length(self, n):
    # Check edge cases
        n = max(n, 1)

        counter = 1
        while n > 1:
            if (n % 2 == 0):
                n = n//2
            else:
                n = 3*n+1
            counter += 1

        return counter
